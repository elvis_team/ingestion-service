package com.codechallenge.ingestionservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


@Service
public class ProducerService {

	private final Logger log = LoggerFactory.getLogger(ProducerService.class);
	
	/**
	 * KakfaTemplate instances are thread-safe and use of one instance is recommended.
	 */
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	/**
	 * Send request to Kafka Queue
	 * 
	 * @param topic
	 * @param data
	 */
	public void send(String topic, String data) {
		log.info( "***sending data{} to {}", data, topic);
		kafkaTemplate.send(topic, data);
	}
}
