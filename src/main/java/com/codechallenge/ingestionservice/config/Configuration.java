package com.codechallenge.ingestionservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("ingestion-service")
public class Configuration {

	private String deliveryeventtopic;
	private String clickeventtopic;
	private String installeventtopic;
	
	public String getDeliveryeventtopic() {
		return deliveryeventtopic;
	}
	public void setDeliveryeventtopic(String deliveryeventtopic) {
		this.deliveryeventtopic = deliveryeventtopic;
	}
	public String getClickeventtopic() {
		return clickeventtopic;
	}
	public void setClickeventtopic(String clickeventtopic) {
		this.clickeventtopic = clickeventtopic;
	}
	public String getInstalleventtopic() {
		return installeventtopic;
	}
	public void setInstalleventtopic(String installeventtopic) {
		this.installeventtopic = installeventtopic;
	}
	
}
