package com.codechallenge.ingestionservice.model;

import java.util.Date;
import java.util.UUID;

public class AdDTO {
	
	private Integer advertisementId;
	
	private UUID deliveryId;
	
	private UUID clickId;
	
	private UUID installId;
	
	private Date time;
	
	private String browser;
	
	private String os;
	
	private String site;
	
	public AdDTO() {
	}

	public Integer getAdvertisementId() {
		return advertisementId;
	}
	public void setAdvertisementId(Integer advertisementId) {
		this.advertisementId = advertisementId;
	}
	public UUID getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(UUID deliveryId) {
		this.deliveryId = deliveryId;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public UUID getClickId() {
		return clickId;
	}
	public void setClickId(UUID clickId) {
		this.clickId = clickId;
	}
	public UUID getInstallId() {
		return installId;
	}
	public void setInstallId(UUID installId) {
		this.installId = installId;
	}

}
