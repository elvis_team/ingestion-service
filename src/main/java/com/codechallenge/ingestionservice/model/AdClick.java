package com.codechallenge.ingestionservice.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;

public class AdClick {
	
	private final String ID_PATTERN = "([a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12})";
	
	@NotNull(message = "Delivery Id parameter missing!")
	@Pattern(regexp=ID_PATTERN, message="Please enter a correct UUID string!")
	private String deliveryId;
	
	@NotNull(message = "Click Id parameter missing!")
	@Pattern(regexp=ID_PATTERN, message="Please enter a correct UUID string!")
	private String clickId;
	
	@NotNull(message="Time parameter missing!")
	@PastOrPresent(message="Time parameter is in the future!")
	private Date time;
	
	public String getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(String deliveryId) {
		this.deliveryId = deliveryId;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getClickId() {
		return clickId;
	}
	public void setClickId(String clickId) {
		this.clickId = clickId;
	}

}
