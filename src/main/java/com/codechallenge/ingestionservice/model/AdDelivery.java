package com.codechallenge.ingestionservice.model;

import java.util.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class AdDelivery {
	
	private final String ID_PATTERN = "([a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12})";
	private final String URI_PATTERN = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	
	@NotNull(message="Advertisement Id parameter missing!")
	@Digits(integer=11, fraction=0, message="Advertisement Id format exception!")
	private Integer advertisementId;
	
	@NotNull(message = "Name parameter missing!")
	@Pattern(regexp=ID_PATTERN, message="Please enter your correct deliveryId format!")
	private String deliveryId;
	
	@NotNull(message="Time parameter missing!")
	@PastOrPresent(message="Time parameter is in the future!")
	private Date time;
	
	@NotNull(message = "Browser parameter missing!")
	@Size(min = 3, message = "Browser letter count too small!")
	private String browser;
	
	@NotNull(message = "OS parameter missing!")
	@Size(min = 2, message = "OS letter count too small!")
	private String os;
	
	@NotNull(message = "Site parameter missing!")
	@Pattern(regexp=URI_PATTERN, message="Enter full site address!")
	private String site;
	
	public Integer getAdvertisementId() {
		return advertisementId;
	}
	public void setAdvertisementId(Integer advertisementId) {
		this.advertisementId = advertisementId;
	}
	public String getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(String deliveryId) {
		this.deliveryId = deliveryId;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}

}
