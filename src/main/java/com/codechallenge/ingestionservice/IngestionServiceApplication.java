package com.codechallenge.ingestionservice;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaAdmin;
import com.codechallenge.ingestionservice.config.Configuration;


@SpringBootApplication
@EnableDiscoveryClient
public class IngestionServiceApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(IngestionServiceApplication.class);
	
	@Autowired
    private Configuration configuration;
	
	@Value("${spring.kafka.bootstrap-servers}")
	private String kafkaBrokers; // comma separated brokers
	
	public static void main(String[] args) {
		SpringApplication.run(IngestionServiceApplication.class, args);
	}
	
	@Bean
	public KafkaAdmin admin() {
	    Map<String, Object> configs = new HashMap<>();
	    LOGGER.info("*** Setting Up Kafka Broker {}", System.getenv("SPRING_KAFKA_BOOTSTRAP-SERVERS"));
	    configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers);
	    return new KafkaAdmin(configs);
	}

	@Bean
	public NewTopic createDeliveryTopic() {
		LOGGER.info("*** Creating Topic {}", configuration.getDeliveryeventtopic());
	    return new NewTopic(configuration.getDeliveryeventtopic(), 3, (short) 1);
	}

	@Bean
	public NewTopic createClickTopic() {
		LOGGER.info("*** Creating Topic {}", configuration.getClickeventtopic());
	    return new NewTopic(configuration.getClickeventtopic(), 3, (short) 1);
	}
	
	@Bean
	public NewTopic createInstallTopic() {
		LOGGER.info("*** Creating topic {}", configuration.getInstalleventtopic());
		return new NewTopic(configuration.getInstalleventtopic(), 3, (short) 1);
	}
	
}
