package com.codechallenge.ingestionservice.controller;

import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codechallenge.ingestionservice.config.Configuration;
import com.codechallenge.ingestionservice.model.AdClick;
import com.codechallenge.ingestionservice.model.AdDTO;
import com.codechallenge.ingestionservice.model.AdDelivery;
import com.codechallenge.ingestionservice.model.AdInstall;
import com.codechallenge.ingestionservice.model.ResponseWrapper;
import com.codechallenge.ingestionservice.service.ProducerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/ingestion")
public class IngestionController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static ObjectMapper mapper = null;
	
	@Autowired
	private ProducerService producerService;
	
	@Autowired
	private Configuration configuration;
	
	@PostMapping(value="/delivery", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> sendDeliveryEvent(@Valid @RequestBody AdDelivery adDelivery, Errors errors) throws JsonProcessingException {
		logger.info("Posting ads delivery ... to {}", configuration.getDeliveryeventtopic());
		if (errors.hasErrors()) {
			logger.error("Error in delivery body detected...{}", errors.getAllErrors());
			throw new ValidationException(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(", ")));
		}
		// Parse AdDelivery to AdDTO
		AdDTO adDto = getAdDTO(adDelivery);
		String adDeliveryJsonString = getSingleMapper().writeValueAsString(adDto);
		// Push event to queue
		producerService.send(configuration.getDeliveryeventtopic(), adDeliveryJsonString);
		logger.info("Returning ads delivery response...");
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Delivery event sent."), HttpStatus.OK);
	}
	
	@PostMapping(value="/click", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> sendClickEvent(@Valid @RequestBody AdClick adClick, Errors errors) throws JsonProcessingException {
		logger.info("Posting ads click...");
		if (errors.hasErrors()) {
			logger.error("Error in click body detected...");
			throw new ValidationException(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(", ")));
		}
		// Parse AdClick to AdDTO
		AdDTO adDto = getAdDTO(adClick);
		String adClickJsonString = getSingleMapper().writeValueAsString(adDto);
		// Push event to queue
		producerService.send(configuration.getClickeventtopic(), adClickJsonString);
		logger.info("Returning ads click response...");
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Click event sent."), HttpStatus.OK);
	}
	
	@PostMapping(value="/install", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> sendInstallEvent(@Valid @RequestBody AdInstall adInstall, Errors errors) throws JsonProcessingException {
		logger.info("Posting ads install...");
		if (errors.hasErrors()) {
			logger.error("Error in install body detected...");
			throw new ValidationException(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(", ")));
		}
		// Parse AdClick to AdDTO
		AdDTO adDto = getAdDTO(adInstall);
		String adInstallJsonString = getSingleMapper().writeValueAsString(adDto);
		// Push event to queue
		producerService.send(configuration.getInstalleventtopic(), adInstallJsonString);
		logger.info("Returning ads install response...");
		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Install event sent."), HttpStatus.OK);
	}
	

	private AdDTO getAdDTO(Object o) {
		AdDTO adDto = new AdDTO();
		if(o instanceof AdDelivery ) {
			AdDelivery adDelivery = (AdDelivery) o;
			adDto.setAdvertisementId(adDelivery.getAdvertisementId());
			adDto.setTime(adDelivery.getTime());
			adDto.setBrowser(adDelivery.getBrowser());
			adDto.setOs(adDelivery.getOs());
			adDto.setDeliveryId(UUID.fromString(adDelivery.getDeliveryId()));
			adDto.setSite(adDelivery.getSite());
		}
		if(o instanceof AdClick ) {
			AdClick adClick = (AdClick) o;
			adDto.setTime(adClick.getTime());
			adDto.setBrowser("undefine");
			adDto.setOs("undefine");
			adDto.setClickId(UUID.fromString(adClick.getClickId()));
		}
		if(o instanceof AdInstall ) {
			AdInstall adInstall = (AdInstall) o;
			adDto.setTime(adInstall.getTime());
			adDto.setBrowser("undefine");
			adDto.setOs("undefine");
			adDto.setInstallId(UUID.fromString(adInstall.getInstallId()));
		}
		return adDto;
	}
	
	public ObjectMapper getSingleMapper() {
       if(mapper == null) {
    	   logger.info("Returning new Object Mapper...");
    	   mapper = new ObjectMapper();
       } else {
    	   logger.info("Returning Object Mapper...");
       }
       return mapper;
    }
	
}
