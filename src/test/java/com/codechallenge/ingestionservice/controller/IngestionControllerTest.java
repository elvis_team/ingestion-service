package com.codechallenge.ingestionservice.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import javax.validation.ValidationException;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.codechallenge.ingestionservice.IngestionServiceApplication;
import com.codechallenge.ingestionservice.config.Configuration;
import com.codechallenge.ingestionservice.service.ProducerService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IngestionServiceApplication.class)
public class IngestionControllerTest {

	private MockMvc mockMvc;

    @Mock
    private ProducerService producerService;

    @Mock
	private Configuration configuration;
    
    @InjectMocks
    private IngestionController ingestionController;
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(ingestionController).build();
    }
	
	@Test
	public void sendDeliveryEventTest() throws Exception {
		String json = "{\"advertisementId\": 4483,\"deliveryId\": \"244cf0db-ba28-4c5f-8c9c-2bf11ee42988\",\"clickId\":null,\"installId\":null,\"time\": \"2018-01-07T18:32:23.602300+0000\",\"browser\": \"Chrome\",\"os\": \"iOS\",\"site\": \"http://super-dooper-news.com\"}";
		doNothing().when(producerService).send(Mockito.anyString(), Mockito.anyString());
		Mockito.when(
				configuration.getDeliveryeventtopic()
				).thenReturn("deliveryeventtopic");
		mockMvc.perform(post("/ingestion/delivery")
			.contentType(MediaType.APPLICATION_JSON)
	        .content(json))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.status", Matchers.is(HttpStatus.OK.value())))
            .andExpect(jsonPath("$.message", Matchers.is("Delivery event sent.")))
            .andExpect(jsonPath("$.*", Matchers.hasSize(2)));
	}
	
	@Test
	public void sendDeliveryEventViolationExceptionTest() throws Exception {
		
        thrown.expectCause(isA(ValidationException.class));

        String json = "{\n\"advertisementId\": 4483,\n\"deliveryId\": \"244cf0db-ba28-4c5f\",\n\"time\": \"2018-01-07T18:32:23.602300+0000\",\n\"browser\": \"Chrome\",\n\"os\": \"iOS\",\n\"site\": \"http://super-dooper-news.com\"\n}";
		mockMvc.perform(post("/ingestion/delivery")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json));
	}
	
	@Test
	public void sendClickEventTest() throws Exception {
		String json = "{\"deliveryId\": \"244cf0db-ba28-4c5f-8c9c-2bf11ee42988\", \"clickId\" : \"fff54b83-49ff-476f-8bfb-2ec22b252c32\", \"time\": \"2018-01-07T18:32:34.201100+0000\"}";
		doNothing().when(producerService).send(Mockito.anyString(), Mockito.anyString());
		Mockito.when(
				configuration.getClickeventtopic()
				).thenReturn("clickeventtopic");
		mockMvc.perform(post("/ingestion/click")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status", Matchers.is(HttpStatus.OK.value())))
		.andExpect(jsonPath("$.message", Matchers.is("Click event sent.")))
		.andExpect(jsonPath("$.*", Matchers.hasSize(2)));
	}
	
	@Test
	public void sendClickEventViolationExceptionTest() throws Exception {
		
		thrown.expectCause(isA(ValidationException.class));
		
		String json = "{\"deliveryId\": \"4cf0db-ba28-4c5f-8c9c-2bf11ee42988\", \"clickId\" : \"fff54b83-49ff-476f-8bfb-2ec22b252c32\", \"time\": \"2018-01-07T18:32:34.201100+0000\"}";
		mockMvc.perform(post("/ingestion/click")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json));
	}
	
	@Test
	public void sendInstallEventTest() throws Exception {
		String json = "{\"installId\": \"144cf0db-ba28-4c5f-8c9c-2bf11ee42988\", \"clickId\" : \"fff54b83-49ff-476f-8bfb-2ec22b252c32\", \"time\": \"2018-01-07T18:32:34.201100+0000\"}";
		doNothing().when(producerService).send(Mockito.anyString(), Mockito.anyString());
		Mockito.when(
				configuration.getInstalleventtopic()
				).thenReturn("installeventtopic");
		mockMvc.perform(post("/ingestion/install")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status", Matchers.is(HttpStatus.OK.value())))
		.andExpect(jsonPath("$.message", Matchers.is("Install event sent.")))
		.andExpect(jsonPath("$.*", Matchers.hasSize(2)));
	}
	
	@Test
	public void sendInstallEventViolationExceptionTest() throws Exception {
		
		thrown.expectCause(isA(ValidationException.class));
		
		String json = "{\"installId\": \"144cf0db-ba28-4c5f-8c9c-2bf11ee42988\", \"clickId\" : \"f54b83-49ff-476f-8bfb-2ec22b252c32\", \"time\": \"2018-01-07T18:32:34.201100+0000\"}";
		mockMvc.perform(post("/ingestion/install")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json));
	}
	
}
